# Tax scrape
  The script collects information about taxes.
## Getting started
  ```ruby
  rvm use 2.5.1
  bundle install
  ./taxes.rb ~/Downloads/Tax.xlsx  
  ```
> The input file must be an xlsx (Excel) file.

> After the script is completed, the file "taxes.csv" will be created in the root of the directory.