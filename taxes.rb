#!/usr/bin/env ruby

require './city'
require 'rubygems'
require 'ostruct'
require 'progressbar'
require 'httparty'
require 'nokogiri'
require 'roo'

class TaxScraper
  include City
  include HTTParty

  base_uri 'http://tax1.co.monmouth.nj.us'

  attr_reader :data, :debug, :progressbar, :agent, :upload_dir

  def initialize(rows_count, debug = false, upload_dir = nil)
    @debug = debug
    @progressbar = ProgressBar.create(total: rows_count, title: 'Scraping taxes', format: "%t: |%B| %c from %C (%p)")
    @upload_dir = upload_dir || File.expand_path('~/Desktop/taxes.csv')
    @data = []
  end

  def scrape(hash)
    city = CITIES.find { |c| c[:name] =~ /#{hash[:city]}/i  }

    unless city
      puts "\n#{hash[:city]} not found" if debug
      data << { location: hash[:location], city: hash[:city], tax: 'city not found' }
      return
    end
    
    address = OpenStruct.new(city.merge(hash))
    
    begin
      retries ||= 0
      show_info_page = find_location(address)
    rescue
      if (retries += 1) < 2
        puts "\ntry ##{address.location}" if debug
        location = try_location(address)
        address.location = location if location

        return unless location
        retry
      else
        data << { location: address.location, city: address.city, tax: 'address not found' }
        return
      end
    end

    unless show_info_page.success?
      puts "##show_info_page: #{address.location} - address not found"
      data << { location: address.location, city: address.city, tax: 'address not found' }      
      return
    end

    tax = Nokogiri(show_info_page.body).xpath("//td/font[contains(text(), 'Taxes:')]/parent::td/following-sibling::td/font").text
    tax = tax.gsub("\n", '').chop
    data << { location: address.location, city: address.city, tax: tax }
    
    progressbar.increment
  end

  def find_location(address)
    search_result_page = self.class.post('/cgi-bin/inf.cgi', body: search_params(address))
    show_info_url = Nokogiri.parse(search_result_page.body).css('a/@href').text

    raise 'Address not fount' if show_info_url.empty?
    
    self.class.get("/cgi-bin/#{show_info_url}")
  end

  def try_location(address)
    location = address.location
    city = address.city

    location =~ /#{ABBR.keys.join('|')}/i
    abbr = Regexp.last_match.to_s
    
    if abbr.empty?
      puts "#{location} - abbr not found" if debug
      data << { location: location, city: city, tax: 'address not found' }
      return
    end

    location.gsub(abbr, ABBR[abbr.downcase.to_sym])
  end

  def search_params(address)
    {
      ms_user: 'monm', 
      passwd: '', 
      srch_type: 1, 
      select_cc: address.county_id, 
      district: address.city_id, 
      adv: 1,
      out_type: 1, 
      ms_ln: 50, 
      p_loc: address.location, 
      owner: '', 
      block: '', 
      lot: '', 
      qual: ''
    }
  end

  def csv_export!
    CSV.open(upload_dir, "w") do |csv|
      csv << ["ST NAME", "CITY", "TAX"]
      data.each do |item|
        csv << item.values
      end
    end
  end
end

xlsx = Roo::Spreadsheet.open(ARGV[0])
rows_count = xlsx.sheet(0).drop(1).count
tax_scraper = TaxScraper.new(rows_count, false)
xlsx.sheet(0).each(location: /ADDRESS/i, city: /CITY/i).drop(1).each do |hash|
  next unless hash[:location]
  tax_scraper.scrape(hash)
end

tax_scraper.csv_export!


